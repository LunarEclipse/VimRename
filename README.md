# VimRename
VimRename is a batch rename tool written in Python 3.6.  
It works by opening a file list (one file per line, prepended with an ID) in a text editor (`vim` by default), letting the user edit the file names, and renaming the files accordingly once the editor exits.  
Vim can work really well for this task, as you can use macros, substitutions, and other tools it provides to edit the filenames.

# Usage
You will need Python 3.6 or newer.  
Run `python3 vimrename.py --help` to see the help page.  
The basic usage is `python3 vimrename.py <directory>` which will let you rename the contents of the specified directory.  
The program will guide you through the entire process.  

# License
This project is licensed under the [European Union Public Licence, Version 1.2 or later (EUPL)](https://www.eupl.eu/1.2/en/)
