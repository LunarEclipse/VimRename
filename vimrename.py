#!/usr/bin/env python3
#
# Copyright 2022 Luna Ćwięka
#
import re
import subprocess
import sys
import os
import shutil
from math import log10, trunc
from os import listdir, mkdir, rename
from os.path import dirname, join
from tempfile import NamedTemporaryFile
from typing import Dict, List, Tuple, Any

PREFIX_RE_PATTERN = re.compile(r"^\d+: ")


def find_editor() -> str:
    preferred = os.getenv("EDITOR")
    defaults = ["nvim", "vim", "nano", "vi"]
    if preferred is not None:
        return preferred
    else:
        try:
            return [e for e in defaults if shutil.which(e) is not None][0]
        except IndexError:
            print("Error: $EDITOR not set and couldn't find any known text editor!", file=sys.stderr)
            exit(1)

EDITOR = find_editor()


def rename_folder(dir="."):
    # filelist is List[Any] instead of List[Tuple[int, str]] to avoid silly type warnings from PyCharm
    # yes, I know it's path-like objects and not strings, but for my use they're equivalent to strings okay?
    def encode_filelist(filelist: List[Any]) -> str:
        if len(filelist) == 0:
            return ""
        files = ""
        pfx_width = trunc(log10(len(filelist))) + 1  # Zero-padded prefix, only as wide as necessary
        for i, d in filelist:
            files += f"{i:0>{pfx_width}}: {d}\n"
        return files

    # TODO: Add custom editor support?
    def edit_in_vim(text: str) -> str:
        with NamedTemporaryFile() as tmp:
            tmp.write(text.encode())
            tmp.seek(0)
            subprocess.run([EDITOR, tmp.name], check=True)
            tmp.seek(0)
            return tmp.read().decode()

    def decode_filelist(encoded: str) -> List[Tuple[int, str]]:
        def split_prefix_and_name(prefixed: str) -> Tuple[int, str]:
            pfx = prefixed.split(": ", 1)
            return int(pfx[0]), pfx[1]

        def validate_prefixed_line(line: str) -> bool:
            if line == "":
                return False
            elif PREFIX_RE_PATTERN.search(line) is None:
                print(f"!> Ignoring invalid line: \"{line.strip()}\"", file=sys.stderr)
                return False
            else:
                return True

        return list(map(split_prefix_and_name, filter(validate_prefixed_line, encoded.split("\n"))))

    # noinspection PyShadowingNames
    # orig_filelist is List[Any] instead of List[Tuple[int, str]] to avoid silly type warnings from PyCharm
    def create_actions(orig_filelist: List[Any], new_filelist: List[Tuple[int, str]]) -> Dict[str, str]:
        print(f"\nPending Actions in folder \"{dir}\":\n")
        actions = dict()
        for i, new_file in new_filelist:
            if i < len(orig_filelist):
                orig_file = orig_filelist[i][1]
                if orig_file in actions:
                    print(f"!> Ignoring prior entry of duplicate index: \"{i}\"\n"
                          f"!> Ignored action: \"{orig_file}\" -> \"{actions[orig_file]}\"")
                    print()
                actions[orig_file] = new_file
            else:
                print(f"!> Ignoring line with invalid index: \"{i}\": \"{new_file}\"", file=sys.stderr)
                print()
        for old, new in actions.items():
            if old != new:
                print(f"-> \"{old}\"")
                print(f"+> \"{new}\"")
                print()
        print()
        return actions

    raw_filelist = sorted(listdir(dir))
    orig_filelist = list(enumerate(raw_filelist))
    orig_files = encode_filelist(orig_filelist)
    new_files = edit_in_vim(orig_files)
    new_filelist = decode_filelist(new_files)

    while True:
        actions = create_actions(orig_filelist, new_filelist)
        print("Please confirm (Y/n/s/r/d/?): ", end="")

        # Yes - rename files
        if (choice := input().strip().lower()) == "y":
            for old, new in actions.items():
                if old == new:
                    continue
                # TODO: find out if it wouldn't be better to just always mkdir here
                try:
                    rename(join(dir, old), join(dir, new))
                except FileNotFoundError:
                    mkdir(dirname(join(dir, new)))
                    rename(join(dir, old), join(dir, new))
            break
        # Quit - exit program
        elif choice == "n":
            print("User chose to quit.")
            exit(1)
        # Skip - skip current folder
        elif choice == "s":
            print("Skipping folder.")
            break
        # Retry - reopen editor keeping previous changes
        elif choice == "r":
            new_files = edit_in_vim(new_files)
            new_filelist = decode_filelist(new_files)
            continue
        # Discard - discard changes and retry
        elif choice == "d":
            new_files = edit_in_vim(orig_files)
            new_filelist = decode_filelist(new_files)
            continue
        # ? - display help
        elif choice == "?":
            print()
            print("?> Y: yes; n: abort; s: skip this folder")
            print("?> r: retry; d: discard and retry")
            print("?> ?: help")
            print()
            input("Press enter to continue...")
            continue
        else:
            print("Please select an option!")
            print("Press enter to retry...")
            input()
            print("\033[A" + "\033[2K" + "\033[A" + "\033[2K", end="")
            continue


def print_usage():
    print("Usage: vimrename [OPTION]... [FOLDER]...")
    print("Open the filelist(s) of FOLDER(s) in vim for easy mass-renaming.")
    print("\nWith no FOLDER, assumes $PWD.\n")
    print("OPTIONS: ")
    print("    -h | --help      : display this help page and exit")
    # TODO print("    -c | --chained   : opens everything together in a single editor instance")
    # TODO print("         --noconfirm : doesn't ask for confirmation")
    # TODO print("    -r | --recursive : opens all subdirectories")


# TODO: add a way to distinguish files from dirs ("00F:", "00D:")
# TODO: add a way to save your edits if the program crashes and pick back up again later
def main():
    args = sys.argv[1:]
    if len(args) == 0:  # Use $PWD as default folder
        rename_folder(".")
    elif args[0] == "-h" or args[0] == "--help":
        print_usage()
        exit(1)
    # TODO: add an elif for stdin folder input with -
    elif len(args) == 1:  # Single folder
        rename_folder(args[0])
    else:  # Multiple folders
        for i, path in enumerate(args):
            # TODO: check if patch is actually a folder and skip files
            i_width = trunc(log10(len(args))) + 1
            print(f"Next up folder {i + 1:>{i_width}}/{len(args)} - \"{path}\"")

            print("Press enter to continue, type s to skip, or q to quit: ", end="")
            if (choice := input().strip().lower()) == "s":
                print("Skipping folder.")
                continue
            elif choice == "q":
                print("User chose to quit.")
                exit()
            else:
                # https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_(Control_Sequence_Introducer)_sequences
                print("\033[A" + "\033[2K", end="")
                rename_folder(path)
        print("Done :)")


# TODO: Add some logging function with log levels
if __name__ == "__main__":
    main()
